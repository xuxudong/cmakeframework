find_package(Gflags 1 CONFIG QUIET)

# if(Gflags_FOUND)
#   get_property(_loc TARGET gflag::gflag PROPERTY LOCATION)
#   message(STATUS "Found gflag: ${_loc} (found version ${gflag_VERSION})")
#   add_library(gflag_external INTERFACE)  # dummy
# else()
  message(STATUS "Suitable message could not be located, Building message instead.")

  include(ExternalProject)
  list(APPEND CMAKE_CXX_FLAGS "-fPIC")
  ExternalProject_Add(gflags_external
    GIT_REPOSITORY
        https://gitee.com/xuxudong/gflags.git
    GIT_TAG
      master
    UPDATE_COMMAND
      ""
    CMAKE_ARGS
      -DCMAKE_INSTALL_PREFIX=${STAGED_INSTALL_PREFIX}
      -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
      -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
      -DCMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}
      -DCMAKE_CXX_EXTENSIONS=${CMAKE_CXX_EXTENSIONS}
      -DCMAKE_CXX_STANDARD_REQUIRED=${CMAKE_CXX_STANDARD_REQUIRED}
      -DGFLAGS_NAMESPACE=gflags
      -DBUILD_TESTING=OFF
    CMAKE_CACHE_ARGS
      -DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
    # TEST_AFTER_INSTALL
    #   1
    DOWNLOAD_NO_PROGRESS
      1
    LOG_CONFIGURE
      1
    LOG_BUILD
      1
    LOG_INSTALL
      1
    BUILD_ALWAYS
      0
    )

  if(WIN32 AND NOT CYGWIN)
    set(DEF_gflags_DIR ${STAGED_INSTALL_PREFIX}/CMake)
  else()
    set(DEF_gflags_DIR ${STAGED_INSTALL_PREFIX}/lib/cmake/gflags)
  endif()

  message(STATUS ${DEF_gflags_DIR})

  file(TO_NATIVE_PATH "${DEF_gflags_DIR}" DEF_gflags_DIR)

  set(gflags_DIR ${DEF_gflags_DIR}
      CACHE PATH "Path to internally built messageConfig.cmake" FORCE)
# endif()