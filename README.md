# cmake 工程模板

目前包含必要的 日志，参数，单元测试，后期慢慢增加

## 主目录结构

```bash
3rdpart :存放引用的第三方库
build	:构建目录，需自己创建
package	:存放打包脚本
cmake	:存放工具链，供交叉编译使用
src		:本工程源码目录
external:外部项目的配置目录,主要包含glog,gflag（无需更改）
tests	:测试目录，单元测试（内部使用gtest）
```

## 构建

**注：cmake 版本>=3.16**

本机编译

`mkdir build && cd build && cmake ..  && make -j8`

交叉编译

`mkdir build && cd build && cmake -DCAMKE_TOOLCHAIN_FILE=../cmake/aarch64_7.cmake  .. && make -j8`

