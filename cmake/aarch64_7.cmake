    # 设置目标系统
    set(CMAKE_SYSTEM_NAME Linux)
    set(CMAKE_SYSTEM_PROCESSOR arm)
    # 设置安装目录
    #set(CMAKE_STAGING_PREFIX /opt/gcc-linaro-5.4.1-2017.01-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/usr/local)
    SET(CMAKE_CXX_FLAGS_DEBUG"$ENV{CXXFLAGS} -O0 -Wall -g -ggdb")
    SET(CMAKE_CXX_FLAGS_RELEASE"$ENV{CXXFLAGS} -O3 -Wall")
    
    SET(INSTALL /opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/usr/local)
    
    # 设置工具链目录
    set(TOOL_CHAIN_DIR /opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu)
    set(TOOL_CHAIN_INCLUDE
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/include
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/include
        )
    
    set(TOOL_CHAIN_LIB
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/lib
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/cuda-9.0/lib
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/cuda-9.0/targets/aarch64-linux/lib
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/tegra
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/lib
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/libc/lib/aarch64-linux-gnu
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/libc/usr/lib/aarch64-linux-gnu
        ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/libc/lib
        )
    
#set(ZLIB_INCLUDE_DIR ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/include)
#    set(ZLIB_LIBRARY ${TOOL_CHAIN_DIR}/aarch64-linux-gnu/usr/local/lib/libz.so.1)
    
    # 设置编译器位置
    set(CMAKE_C_COMPILER ${TOOL_CHAIN_DIR}/bin/aarch64-linux-gnu-gcc)
    set(CMAKE_CXX_COMPILER ${TOOL_CHAIN_DIR}/bin/aarch64-linux-gnu-g++)
    
    # 设置Cmake查找主路径
    set(CMAKE_FIND_ROOT_PATH ${TOOL_CHAIN_DIR}/aarch64-linux-gnu)
    
    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    # 只在指定目录下查找库文件
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    # 只在指定目录下查找头文件
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    # 只在指定目录下查找依赖包
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
    
    include_directories(${TOOL_CHAIN_INCLUDE})
    link_directories(${TOOL_CHAIN_LIB})
    set(CMAKE_INCLUDE_PATH ${TOOL_CHAIN_INCLUDE})
    set(CMAKE_LIBRARY_PATH ${TOOL_CHAIN_LIB})
#    set(ENV{PKG_CONFIG_PATH} /opt/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu/aarch64-linux-gnu/usr/local/lib/pkgconfig)
#    find_package(PkgConfig) 
