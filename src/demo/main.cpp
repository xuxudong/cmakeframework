/**
 * @file main.cpp
 * @author xudongxu (1539731480@qq.com)
 * @brief 
 * @version 0.1
 * @date 2020-08-25
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <cstdlib>
#include <iostream>
#include <string>
#include <glog/logging.h>
#include <gflags/gflags.h>

DEFINE_bool(log2file, false, "log write to file");
/**
 * @brief 
 * 
 * @return std::string 
 */
std::string say_hello() { return std::string("Hello, CMake superbuild world!"); }

int main(int argc,char** argv)
{
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  if (!FLAGS_log2file)
  {
      std::cout<<"log to console ...\n";
      FLAGS_logtostderr = true;  
      FLAGS_alsologtostderr = true;  
      FLAGS_colorlogtostderr = true;  
      FLAGS_log_prefix = true;  
      FLAGS_logbufsecs = 0;  //0 means realtime
      FLAGS_max_log_size = 10;  // MB
  }else
    std::cout<<"log to file ...\n";
  google::InitGoogleLogging(argv[0]); // init google logging
  google::SetLogDestination(google::GLOG_FATAL, "./log/log_fatal_"); 
  google::SetLogDestination(google::GLOG_ERROR, "./log/log_error_"); 
  google::SetLogDestination(google::GLOG_WARNING, "./log/log_warning_");
  google::SetLogDestination(google::GLOG_INFO, "./log/log_info_");

  //--------------------------------------------------- MAIN -----------------------------------------------------------------
  
  LOG(INFO) << "Hello GLOG";
  
  //--------------------------------------------------- MAIN -----------------------------------------------------------------

  gflags::ShutDownCommandLineFlags();
  return EXIT_SUCCESS;
}